<?php
	require_once 'head.php';
	require_once 'bdd.php';

	

	if (!empty($_POST)){

		$req = $pdo->prepare("
		UPDATE personnes
		SET pers_nom=:nom, pers_prenom=:prenom, pers_age=:age
		WHERE pers_id=:id
		");

		$req->execute([
			'prenom' => $_POST['prenom'],
			'nom' => $_POST['nom'],
			'age' => $_POST['age'],
			'id' => $_POST['id']
		]);

		header('location:index.php');

	} else {
		$req = $pdo->prepare('SELECT * FROM personnes
		WHERE pers_id=:id');

	$req->execute([
		'id' => $_GET['id']
	]);
	$row = $req->fetch(PDO::FETCH_ASSOC);
	}

?>

<h1>Modifier un contact</h1>
<form method="post" action="modifier.php">
	<fieldset>
		<div>
			<label for="nom">Nom&nbsp;:</label>
			<input type="text" id="nom" name="nom" value="<?=$row['pers_nom']?>">
		</div>
		<div>
			<label for="prenom">Prénom&nbsp;:</label>
			<input type="text" id="prenom" name="prenom" value="<?=$row['pers_prenom']?>"/>
		</div>
		<div>
			<label for="age">Age&nbsp;:</label>
			<input type="text" id="age" name="age" value="<?=$row['pers_age']?>"/>
		</div>
		<input type="hidden" name="id" value="<?= $row['pers_id'] ?>"/>
		<input type="reset" value="Annuler"/>
		<input type="submit" value="Modifier"/>
	</fieldset>
</form>

<ul>
	<li><a href="index.php">Retour au sommaire</a></li>
	<li><a href="afficher.php">Retour à la liste des contacts</a></li>
</ul>


</body>
</html>