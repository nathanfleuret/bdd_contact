<?php
	require_once 'head.php';
	require_once 'bdd.php';

	$result = $pdo->query('SELECT * FROM personnes');
	$NbreData = $result->rowCount();
	$rowAll = $result->fetchAll(PDO::FETCH_ASSOC);
?>

<h1>Liste des contacts</h1>
<a href="index.php">Retour au sommaire</a>

<table border="1px">
	<th>Nom</th>
	<th>Prénom</th>
	<th>Age</th>
	<th>Modifier</th>
	<th>Supprimer</th>
	
	<?php  
		foreach ( $rowAll as $row )  {
	?>

	<tr>
		<td><?= $row['pers_nom']?></td>
		<td><?= $row['pers_prenom']?></td>
		<td><?= $row['pers_age']?></td>
		<td><a href="modifier.php?id=<?= $row['pers_id']?>">Modifier</a>
		<td><a href="supprimer.php?id=<?= $row['pers_id']?>" onclick="return confirm('Êtes-vous sûr de vouloir supprimer?')">Supprimer</a></td>
	</tr>
	
	<?php
		}
	?>
	
</table>

</body>
</html>