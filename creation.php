<?php
require_once 'head.php';
require_once 'bdd.php';


if (!empty($_POST)){

	$req = $pdo->prepare("
	INSERT INTO personnes (pers_prenom, pers_nom, pers_age) 
	VALUES (:prenom, :nom, :age)
	");

	$req->execute([
		'prenom' => $_POST['prenom'],
		'nom' => $_POST['nom'],
		'age' => empty($_POST['age']) ? null : $_POST['age']
	]);

	header('location:index.php');

}

?>

<h1>Ajout d'un contact</h1>
<form method="post" action="creation.php">
	<fieldset>
		<div>
			<label for="nom">Nom&nbsp;:</label>
			<input type="text" id="nom" name="nom" placeholder="Veuillez saisir votre nom"/>
		</div>
		<div>
			<label for="prenom">Prénom&nbsp;:</label>
			<input type="text" id="prenom" name="prenom" placeholder="Veuillez saisir votre prénom"/>
		</div>
		<div>
			<label for="age">Age&nbsp;:</label>
			<input type="text" id="age" name="age" placeholder="Veuillez saisir votre age"/>
		</div>
		<input type="reset" value="Effacer"/>
		<input type="submit" value="Valider"/>
	</fieldset>
</form>
<a href="index.php">Retour au sommaire</a>

</body>
</html>